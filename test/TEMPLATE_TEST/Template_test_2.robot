*** Settings ***
Documentation     A test suite with a single test : navigation, Browser in dataset in Squash
Library     Browser
Library    squash_tf.TFParamService
Test Template    Navigate To Url

*** Keywords ***
Navigate To Url
    [Arguments]    ${url}
    ${browser} =	Get Test Param	DS_browser
    New Browser    browser=${browser}   headless=True
    New Page    ${url}
    Set Viewport Size    1800    1000
    Close Browser

*** Test Cases ***
Test Linkedin    https://www.linkedin.com    
Test Facebook    https://www.facebook.com    
 Test Yahoo    https://www.yahoo.com
