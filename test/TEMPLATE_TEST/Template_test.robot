*** Settings ***
Documentation     A test suite with a single test : navigation, Browser in dataset in Squash
Library     Browser
Library    squash_tf.TFParamService    

*** Keywords ***
Navigate To ${url}
    ${browser} =	Get Test Param	DS_browser
    New Browser    browser=${browser}   headless=True
    New Page    ${url}
    Set Viewport Size    1800    1000
    Close Browser

*** Test Cases ***
Test Navigation
    [Template]    Navigate To ${url}
    https://www.linkedin.com
    https://www.facebook.com
    https://www.yahoo.com
